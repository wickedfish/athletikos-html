/* #####################################################################
   #
   #   Project       : Modal Login with jQuery Effects
   #   Author        : Rodrigo Amarante (rodrigockamarante)
   #   Version       : 1.0
   #   Created       : 07/29/2015
   #   Last Change   : 08/04/2015
   #
   ##################################################################### */
   
// Initialize Firebase
// Initialize Firebase
var config = {
	apiKey: "AIzaSyAA_k1wpwu6PO6m3TqdtgdLj8nQ5clWCRs",
	authDomain: "athletikos-fb54c.firebaseapp.com",
	databaseURL: "https://athletikos-fb54c.firebaseio.com",
	storageBucket: "athletikos-fb54c.appspot.com",
	messagingSenderId: "931243375094"
};
firebase.initializeApp(config);
var fireData = firebase.database();

function toggleSignIn() {
      if (firebase.auth().currentUser) {
        // [START signout]
        firebase.auth().signOut();
        // [END signout]
      } else {
        var email = document.getElementById('login_username').value;
        var password = document.getElementById('login_password').value;
        if (email.length < 4) {
          alert('Please enter an email address.');
          return;
        }
        if (password.length < 4) {
          alert('Please enter a password.');
          return;
        }
        // Sign in with email and pass.
        // [START authwithemail]
        firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          // [START_EXCLUDE]
          if (errorCode === 'auth/wrong-password') {
            alert('Wrong password.');
          } else {
			  alert("Account doesn't exist")
            //alert(errorMessage);
          }
          console.log(error);
          document.getElementById('quickstart-sign-in').disabled = false;

          // [END_EXCLUDE]
        });
        // [END authwithemail]
      }
		
      //document.getElementById('quickstart-sign-in').disabled = true;
    }

/**
     * Handles the sign up button press.
     */
    function handleSignUp() {
      var email = document.getElementById('register_email').value;
      var password = document.getElementById('register_password').value;
      if (email.length < 4) {
        //alert('Please enter an email address.');
        return;
      }
      if (password.length < 4) {
        //alert('Please enter a password.');
        return;
      }
      // Sign in with email and pass.
      // [START createwithemail]
      firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // [START_EXCLUDE]
        if (errorCode == 'auth/weak-password') {
          alert('The password is too weak.');
        } else {
          alert(errorMessage);
        }
        console.log(error);
        // [END_EXCLUDE]
      });
      // [END createwithemail]
    }

function sendPasswordReset() {
      var email = document.getElementById('lost_email').value;
      // [START sendpasswordemail]
      firebase.auth().sendPasswordResetEmail(email).then(function() {
        // Password Reset Email Sent!
        // [START_EXCLUDE]
        alert('Password Reset Email Sent!');
        // [END_EXCLUDE]
      }).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // [START_EXCLUDE]
        if (errorCode == 'auth/invalid-email') {
          alert(errorMessage);
        } else if (errorCode == 'auth/user-not-found') {
          alert(errorMessage);
        }
        console.log(error);
        // [END_EXCLUDE]
      });
      // [END sendpasswordemail];
    }

function initApp() {
      // Listening for auth state changes.
      // [START authstatelistener]
      firebase.auth().onAuthStateChanged(function(user) {
        // [START_EXCLUDE silent]
        //document.getElementById('quickstart-verify-email').disabled = true;
        // [END_EXCLUDE]
        if (user) {
			if (user.emailVerified) {
			console.log('Email is verified');
		  }
		  else {
			console.log('Email is not verified');
			  user.sendEmailVerification();
			  alert("Email not verified\nVerification email sent")
			  firebase.auth().signOut();
			  return;
		  }
          // User is signed in.
          var displayName = user.displayName;
          var email = user.email;
          var emailVerified = user.emailVerified;
          var photoURL = user.photoURL;
          var isAnonymous = user.isAnonymous;
          var uid = user.uid;
          var providerData = user.providerData;
          // [START_EXCLUDE silent]
			document.getElementsByClassName('LoginButton')[0].textContent = 'Sign out';
			document.getElementsByClassName('LoginButton')[0].addEventListener('click', toggleSignIn, false);
			document.getElementsByClassName('LoginButton')[0].setAttribute('data-toggle', '')
          	document.getElementById('quickstart-sign-in').textContent = 'Sign out';
			document.getElementById('img_logo').setAttribute("src", "images/check.gif")
			setTimeout(function(){
				$("#login-modal").modal('hide');
			}, 1500);
          //document.getElementById('quickstart-account-details').textContent = JSON.stringify(user, null, '  ');
          if (!emailVerified) {
            //document.getElementById('quickstart-verify-email').disabled = false;
          }
          // [END_EXCLUDE]
        } else {
          // User is signed out.
          // [START_EXCLUDE silent]
			document.getElementsByClassName('LoginButton')[0].textContent = 'Sign in';
			document.getElementsByClassName('LoginButton')[0].removeEventListener('click', toggleSignIn, false);
			document.getElementsByClassName('LoginButton')[0].setAttribute('data-toggle', 'modal')
          	document.getElementById('quickstart-sign-in').textContent = 'Sign in';
			document.getElementById('img_logo').setAttribute("src", "http://bootsnipp.com/img/logo.jpg")
          //document.getElementById('quickstart-account-details').textContent = 'null';
          // [END_EXCLUDE]
        }
        // [START_EXCLUDE silent]
        document.getElementById('quickstart-sign-in').disabled = false;
        // [END_EXCLUDE]
      });
      // [END authstatelistener]

      document.getElementById('quickstart-sign-in').addEventListener('click', toggleSignIn, false);
      document.getElementById('quickstart-sign-up').addEventListener('click', handleSignUp, false);
      //document.getElementById('quickstart-verify-email').addEventListener('click', sendEmailVerification, false);
      document.getElementById('quickstart-password-reset').addEventListener('click', sendPasswordReset, false);
    }

    window.onload = function() {
      initApp();
    };

$(function() {
	var $currentForm = $('#login-form');
    var $formLogin = $('#login-form');
    var $formLost = $('#lost-form');
    var $formRegister = $('#register-form');
    var $divForms = $('#div-forms');
    var $modalAnimateTime = 100;
    var $msgAnimateTime = 150;
    var $msgShowTime = 2000;

    $("form").submit(function (e) {
		e.preventDefault();
        switch(this.id) {
            case "login-form":
                var $lg_username=$('#login_username').val();
                var $lg_password=$('#login_password').val();
                if ($lg_username == "ERROR") {
                    msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'), "error", "glyphicon-remove", "Login error");
                }
                return false;
                break;
            case "lost-form":
                var $ls_email=$('#lost_email').val();
                if ($ls_email == "ERROR") {
                    msgChange($('#div-lost-msg'), $('#icon-lost-msg'), $('#text-lost-msg'), "error", "glyphicon-remove", "Send error");
                } else {
                    msgChange($('#div-lost-msg'), $('#icon-lost-msg'), $('#text-lost-msg'), "success", "glyphicon-ok", "Send OK");
                }
                return false;
                break;
            case "register-form":
                var $rg_username=$('#register_username').val();
                var $rg_email=$('#register_email').val();
                var $rg_password=$('#register_password').val();
                if ($rg_username == "ERROR") {
                    msgChange($('#div-register-msg'), $('#icon-register-msg'), $('#text-register-msg'), "error", "glyphicon-remove", "Register error");
                } else {
                    msgChange($('#div-register-msg'), $('#icon-register-msg'), $('#text-register-msg'), "success", "glyphicon-ok", "Register OK");
                }
                return false;
                break;
            default:
                return false;
        }
        return false;
    });
    
    $('#login_register_btn').click( function () { modalAnimate($formLogin, $formRegister); $currentForm=$formRegister;});
    $('#register_login_btn').click( function () { modalAnimate($formRegister, $formLogin); $currentForm=$formLogin;});
    $('#login_lost_btn').click( function () { modalAnimate($formLogin, $formLost); $currentForm=$formLost;});
    $('#lost_login_btn').click( function () { modalAnimate($formLost, $formLogin); $currentForm=$formLogin;});
    $('#lost_register_btn').click( function () { modalAnimate($formLost, $formRegister); $currentForm=$formRegister;});
    $('#register_lost_btn').click( function () { modalAnimate($formRegister, $formLost); $currentForm=$formLost;});
	$('#login-btn').click( function () { 
		setTimeout(function(){
				modalAnimate($currentForm, $formLogin); $currentForm=$formLogin; 
			}, 180);
	});
	//Temporary workaround for getting back to the sign in form. THIS SHOULD BE CHANGED LATER
    
    function modalAnimate ($oldForm, $newForm) {
        var $oldH = $oldForm.height();
        var $newH = $newForm.height();
        $divForms.css("height",$oldH);
        $oldForm.fadeToggle($modalAnimateTime, function(){
            $divForms.animate({height: $newH}, $modalAnimateTime, function(){
                $newForm.fadeToggle($modalAnimateTime);
            });
        });
    }
    
    function msgFade ($msgId, $msgText) {
        $msgId.fadeOut($msgAnimateTime, function() {
            $(this).text($msgText).fadeIn($msgAnimateTime);
        });
    }
    
    function msgChange($divTag, $iconTag, $textTag, $divClass, $iconClass, $msgText) {
        var $msgOld = $divTag.text();
        msgFade($textTag, $msgText);
        $divTag.addClass($divClass);
        $iconTag.removeClass("glyphicon-chevron-right");
        $iconTag.addClass($iconClass + " " + $divClass);
        setTimeout(function() {
            msgFade($textTag, $msgOld);
            $divTag.removeClass($divClass);
            $iconTag.addClass("glyphicon-chevron-right");
            $iconTag.removeClass($iconClass + " " + $divClass);
  		}, $msgShowTime);
    }
});