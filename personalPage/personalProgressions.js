var $workoutExerciseList = $('#workout-exercise-list');
var $workoutInfoModal = $("#workout-info-list");
var $modalAnimateTime = 100;
var $exerciseForm = $("#exercise-form");
var exerciseListForm = $("#goal-input");
var $divForms = $('#div-forms');
var resultsList = $("#results-list");
var removeBool = false;
var fullName = "test";
var user;
$(document).ready(function () {

    //user sensitive information
    firebase.database().ref('/athletikos_data/users/').once('value').then(function (snap1) {
        user = firebase.auth().currentUser.uid;

        //have to nest the calls to force sequential execution
        firebase.database().ref('/athletikos_data/users/' + user + '/').once('value').then(function (snapshot) {
            fullName = snapshot.val().first_name + ' ' + snapshot.val().last_name;
            $('title').text(fullName);
        });

        //populate user goals
        firebase.database().ref('/athletikos_data/users/' + user + '/goals').once('value').then(function (snapshot) {

            var i = 1;
            if (snapshot != null) {
                snapshot.forEach(function (child) {
                    firebase.database().ref('/athletikos_data/users/' + user + '/goals/' + child.key + '/').once('value').then(function (goal) {
                        var currentVal = goal.val().current_val;
                        var targetVal = goal.val().target_val;
                        var startVal = goal.val().start_val;
                        var exercise = goal.val().exercise;
                        var modifier = "";
                        if (goal.val().goal_type == "total_weight") {
                            modifier = "Total weight: "
                        }

                        else if (goal.val().goal_type == "total_reps") {
                            modifier = "Total reps: "
                        }
                        else {
                            modifier = "Max "
                        }


                        $('#goalsRow').append('<div class="col-sm-3 goalsCol colCenter"><h4>' + modifier + " " + exercise + '</h4><div class="progress colCenter" id="progress' + child.key.toString() + '"></div></div>');
                        //setup progress bar
                        var bar = new ProgressBar.Circle('#progress' + child.key.toString(), {
                            color: '#aaa',
                            // This has to be the same size as the maximum width to
                            // prevent clipping
                            strokeWidth: 4,
                            trailWidth: 1,
                            easing: 'easeInOut',
                            duration: 1400,
                            text: {
                                autoStyleContainer: false
                            },
                            from: { color: '#aaa', width: 1 },
                            to: { color: '#034f84', width: 4 },
                            // Set default step function for all animate calls
                            step: function (state, circle) {
                                circle.path.setAttribute('stroke', state.color);
                                circle.path.setAttribute('stroke-width', state.width);

                                var value = (currentVal).toString() + ' of ' + (targetVal).toString();
                                if (value === 0) {
                                    circle.setText('');
                                } else {
                                    circle.setText(value);
                                }

                            }
                        });
                        bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
                        bar.text.style.fontSize = '2rem';
                        //calculate completion percentage
                        var x = (currentVal - startVal) / (targetVal - startVal);
                        bar.animate(x);

                        i += 1;
                    });
                });
            }
        });

        //upcoming workouts
        firebase.database().ref('/athletikos_data/users/' + user + '/workouts').once('value').then(function (snapshot) {
            snapshot.forEach(function (child) {
                firebase.database().ref('/athletikos_data/workouts/' + child.val() + "/").once('value').then(function (snapshot1) {
                    $('#upcoming-workouts').append('<a href="#" data-toggle="modal" data-target="#workout-view" class="list-group-item" id="' + snapshot1.key + '">' + snapshot1.val().workout_name + '</a>');
                });
            });
        });


        //past workouts
        firebase.database().ref('/athletikos_data/users/' + user + '/completed_workouts').once('value').then(function (snapshot) {

            snapshot.forEach(function (child) {
                firebase.database().ref('/athletikos_data/workouts/' + child.key + "/").once('value').then(function (snapshot1) {
                    $('#past-workouts').append('<a href="#" data-toggle="modal" data-target="#workout-view" class="list-group-item" id="' + child.key + '">' + snapshot1.val().workout_name + '</a>');
                });
            });
        });

    });
});







// workout click
$(document.body).on('click', '.list-group-item', function (e) {
    e.preventDefault();
    $("#exercise-list").html("");
    var workoutID = $(this).attr("id");
    firebase.database().ref('/athletikos_data/workouts/' + workoutID + '/exercises/').once('value').then(function ($snapshot) {
        $snapshot.forEach(function (exercise) {
            newTR = '<tr class="ex_table_row"> \
                    <div class ="row"> \
                        <td class="col-md-2 ex_table_item"  id="ex-name-workout-list"><a class="ex-details-btn btn" id="'+ exercise.val().exercise_name + '">' + exercise.val().exercise_name.substring(0,40) + '</a></td> \
                        <td class="col-md-2 ex_table_item"  id="ex-reps-workout-list">'+ exercise.val().reps + '</td> \
                        <td class="col-md-2 ex_table_item"  id="ex-weight-workout-list">'+ exercise.val().weight + '</td> \
                        <td class="col-md-2 ex_table_item"  id="ex-sets-workout-list">'+ exercise.val().sets + '</td> \
                    </div> \
                </tr>';

            $("#exercise-list").append(newTR);
        });
    });

    $workoutExerciseList.fadeToggle($modalAnimateTime);
});

$("#addGoalBtn").click(function (e) {

    e.stopPropagation();

    var buttonID = $(this).attr('id');

    firebase.database().ref('/athletikos_data/exercises/').once('value').then(function ($snapshot) {
        var $newH = exerciseListForm.height();
        $divForms.css("height", exerciseListForm);

        $snapshot.forEach(function (child) {

            $ex_name = child.val().exercise_name;

            newTR = '<tr class="ex_table_row"> \
                                <td class="text-center" id="ex_name"> \
                                    <div class ="row"> \
                                        <div class="col-md-6 ex_table_item pull-left"><a class="goal-ex-btn btn" id="'+ $ex_name + '">' + $ex_name + '</a></div> \
                                        <div class="col-md-6" id="ex_buttons"><a id="'+ $ex_name + '" class="pull-right btn btn-default goal-ex-add-btn">Select Exercise</a></div> \
                                    </div> \
                                </td> \
                            </tr>';

            $("#goal-exercise-list").append(newTR);

        });
    });
    $("#goal-view").modal('show');
});

$(document.body).on('input', '#goalSearch', function (e) {
    var searchVal = $("#goalSearch").val()
    if (searchVal == "") {
        firebase.database().ref('/athletikos_data/exercises/').once('value').then(function ($snapshot) {
            var $newH = exerciseListForm.height();
            $divForms.css("height", exerciseListForm);

            $snapshot.forEach(function (child) {

                $ex_name = child.val().exercise_name;

                newTR = '<tr class="ex_table_row"> \
                                <td class="text-center" id="ex_name"> \
                                    <div class ="row"> \
                                        <div class="col-md-6 ex_table_item pull-left"><a class="goal-ex-btn btn" id="'+ $ex_name + '">' + $ex_name + '</a></div> \
                                        <div class="col-md-6" id="ex_buttons"><a id="'+ $ex_name + '" class="pull-right btn btn-default goal-ex-add-btn">Select Exercise</a></div> \
                                    </div> \
                                </td> \
                            </tr>';

                $("#goal-exercise-list").append(newTR);

            });
        });
    }



    $("#goal-exercise-list").html("");
    firebase.database().ref('/athletikos_data/exercises/' + searchVal + '/').once('value').then(function ($snapshot) {
        if ($snapshot != null && searchVal != "") {
            var $newH = exerciseListForm.height();
            $divForms.css("height", exerciseListForm);



            $ex_name = $snapshot.val().exercise_name;

            newTR = '<tr class="ex_table_row"> \
                                <td class="text-center" id="ex_name"> \
                                    <div class ="row"> \
                                        <div class="col-md-6 ex_table_item pull-left"><a class="goal-ex-btn btn" id="'+ $ex_name + '">' + $ex_name.substring(0,40) + '</a></div> \
                                        <div class="col-md-6" id="ex_buttons"><a id="'+ $ex_name + '" class="pull-right btn btn-default goal-ex-add-btn">Select Exercise</a></div> \
                                    </div> \
                                </td> \
                            </tr>';

            $("#goal-exercise-list").append(newTR);
        }

    });
});


$(document.body).on('click', '.goal-ex-add-btn', function (e) {
    removeBool = false;
    $("#removeGoalBtn").html("Remove");
    $(".progress").removeClass("highlight");
    var modifier = "";
    if ($("#val").val() == "") {
        alert("No value selected.");
        return;
    }
    if ($("#goal-input input[id='totalWeight']:checked").val() == "on") {
        var thing = { 'current_val': 0, 'exercise': e.target.id, 'goal_type': "total_weight", 'start_val': 0, 'target_val': Number($("#val").val()), 'tracker_id': "total weight " + e.target.id };

        var y = firebase.database().ref('/athletikos_data/users/' + user + '/goals/');
        y.push(thing);
    }
    else if ($("#goal-input input[id='maxWeight']:checked").val() == "on") {
        var thing = { 'current_val': 0, 'exercise': e.target.id, 'goal_type': "total_weight", 'start_val': 0, 'target_val': Number($("#val").val()), 'tracker_id': "max " + e.target.id };

        var y = firebase.database().ref('/athletikos_data/users/' + user + '/goals/');
        y.push(thing);
    }
    else if ($("#goal-input input[id='reps']:checked").val() == "on") {
        var thing = { 'current_val': 0, 'exercise': e.target.id, 'goal_type': "total_weight", 'start_val': 0, 'target_val': Number($("#val").val()), 'tracker_id': "total reps " + e.target.id };

        var y = firebase.database().ref('/athletikos_data/users/' + user + '/goals/');
        y.push(thing);
    }
    else {
        alert("No goal type selected.");
    }

    window.location = "PersonalPage.html";

});

$(document.body).on('click', '.teamCell', function (e) {
    e.stopPropagation();
    $("#results-view").modal('show');
    $("#members-results").html("");
    var i = 1;
    var completed = "";
    var workoutID = $(this).attr("id");
    firebase.database().ref('/athletikos_data/teams/' + teamID + '/completed_workouts/' + workoutID + '/').once('value').then(function ($snapshot) {
        //for each member of the team
        $snapshot.forEach(function (member) {
            //go to user table for name
            firebase.database().ref('/athletikos_data/users/' + member.key + '/').once('value').then(function ($user) {
                var x = $user.val().first_name;
                var y = $user.val().last_name;
                var name = x + " " + y;
                if (member.val().workout_completed == true) {
                    completed = "Yes";
                }
                else {
                    completed = "No";
                }
                var newTR = '<tr class="ex_table_row"> \
                    <td id="row_number"> '+ i.toString() + ' </td> \
                    <div class ="row"> \
                        <td class="col-md-5 ex_table_item">'+ name.toString() + '</td> \
                        <td class="col-md-5 ex_table_item">'+ completed.toString() + '</td> \
                    </div> \
                </tr>';

                $("#members-results").append(newTR);
                i = i + 1;
            });
        });
    });


    $("results-list").fadeToggle($modalAnimateTime);

});




$("#close-workout").click(function () {
    $workoutExerciseList.fadeToggle($modalAnimateTime);
    $("#workout-view").modal("toggle");
});

$("#close-results").click(function () {
    resultsList.fadeToggle($modalAnimateTime);
    $("#members-results").html("");
    $("#results-view").modal("toggle");
});
$("#removeGoalBtn").click(function () {
    removeBool = !removeBool;
    if (removeBool) {
        $(".progress").addClass("highlight");
        $("#removeGoalBtn").html("Done");
    }
    else {
        $("#removeGoalBtn").html("Remove");
        $(".progress").removeClass("highlight");
    }
});

$(document.body).on('click', '.goalsCol', function (e) {
    if (removeBool) {
        var goalID = $(this).find('.progress').attr('id');
        //remove "progress from the beginning of the string"
        goalID = goalID.slice(8, goalID.length);
        var rm = new firebase.database().ref('/athletikos_data/users/' + user + '/goals/' + goalID + '/');
        rm.remove(function (error) {

        });


        e.target.closest('.goalsCol').remove();
    }
});



$("#login-btn").click(function () {
    window.location = "MainPage.html";
});

$('#send-workout').click(function () {
    window.location = "YourWorkoutsPage.html";
});

