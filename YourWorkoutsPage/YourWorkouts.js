var $workoutExerciseList = $('#workout-exercise-list');
var $workoutInfoModal = $("#workout-info-list");
var $modalAnimateTime = 100;
var $exerciseForm = $("#exercise-form");
var $divForms = $('#div-forms');
var sendWorkoutID = "";
var $sendWorkoutModal = $("#send-workout");
var selectedTeam = 0;

function loadWorkouts(){
    firebase.database().ref('/athletikos_data/users/'+ firebase.auth().currentUser.uid +'/workouts/').once('value').then(function($snapshot) {
        if($snapshot.val() != null){
            $snapshot.forEach(function(child){
                if(child.val() != "placeholder"){
                    $("#empty-table-row").remove();
                    firebase.database().ref('/athletikos_data/workouts/'+ child.val() +'/').once('value').then(function($snapshot2) {
                        newTR = '<tr class="workout-tr"> \
                            <td>1</td> \
                            <td class="text-center"> \
                                    <div class="col-md-2 ex_table_item"><a class="workout-details-btn btn" data-toggle="modal" data-target="#workout-view" id="'+ child.val() +'">'+ $snapshot2.val().workout_name +'</a></div> \
                                    <span><a id="'+child.val()+'" class="pull-right btn btn-default delete-workout-btn">Delete Workout</a></span> \
                                    <span><a id="'+child.val()+'" class="pull-right btn btn-default edit-ex-btn" href="./WorkoutCreationPage.html">Edit Workout</a></span> \
                                    <span><button id ="'+child.val()+'" data-toggle="modal" data-target="#send-workout-modal" class="pull-right send-workout-btn btn btn-default">Send Workout</button></span> \
                            </td> \
                        </tr>';

                        $("#workout-list").append(newTR);
                    });
                }
            });
        }
    });
}

$("#workout-list").on("click", ".workout-details-btn", function(){
    var workoutID = $(this).attr("id");
    firebase.database().ref('/athletikos_data/workouts/'+ workoutID +'/exercises/').once('value').then(function($snapshot) {
        $snapshot.forEach(function(exercise){
            newTR = '<tr class="ex_table_row"> \
                    <td id="row_number"> 1 </td> \
                    <div class ="row"> \
                        <td class="col-md-2 ex_table_item"  id="ex-name-workout-list"><a class="ex-details-btn btn" id="'+ exercise.val().exercise_name +'">'+ exercise.val().exercise_name +'</a></td> \
                        <td class="col-md-2 ex_table_item"  id="ex-reps-workout-list">'+exercise.val().reps+'</td> \
                        <td class="col-md-2 ex_table_item"  id="ex-weight-workout-list">'+exercise.val().weight+'</td> \
                        <td class="col-md-2 ex_table_item"  id="ex-sets-workout-list">'+exercise.val().sets+'</td> \
                    </div> \
                </tr>';
            
            $("#exercise-list").append(newTR);
        });
    });
    
    $workoutExerciseList.fadeToggle($modalAnimateTime);
});

$("#close-modal").click(function(){
    if($workoutInfoModal.is(':visible')){
        $workoutInfoModal.fadeToggle($modalAnimateTime);
        $("#workout-view").modal("toggle");
    }
    else if($exerciseForm.is(':visible')){
        $workoutExerciseList.fadeToggle($modalAnimateTime);
        $exerciseForm.fadeToggle($modalAnimateTime);
    } else {
        $workoutExerciseList.fadeToggle($modalAnimateTime);
        $("#exercise-list").html("");
        $("#workout-view").modal("toggle");
    }
    $("#workout-name-input").val("");
});

$("#workout-list").on("click", ".delete-workout-btn", function(){
    var workoutID = $(this).attr("id");
    firebase.database().ref('/athletikos_data/users/'+firebase.auth().currentUser.uid+'/workouts/').once('value').then(function($snapshot) {
        $snapshot.forEach(function(child){
           if(child.val() == workoutID){
               firebase.database().ref('/athletikos_data/users/'+firebase.auth().currentUser.uid+'/workouts/'+child.key).remove();
           } 
        });
    });
    
    firebase.database().ref('/athletikos_data/workouts/'+workoutID).remove();
    
    $(this).closest("tr").remove();
    if($("#workout-list").html() == ""){
        $("#tab_logic").after('<div id="empty-table-row"><h2>Your Have Created No Workouts Added</h2></div>');
    }
});

$("#create-workout-btn").click(function(){
    $workoutInfoModal.fadeToggle($modalAnimateTime);
});

$("#exercise-list").on("click", ".ex-details-btn", function(){
    $workoutExerciseList.fadeToggle($modalAnimateTime, function(){
        $divForms.animate({height: $exerciseForm}, $modalAnimateTime, function(){
            $exerciseForm.fadeToggle($modalAnimateTime);
        });
    });
    
    firebase.database().ref('/athletikos_data/exercises/'+ $(this).attr('id') +'/').once('value').then(function($snapshot) {
        populateExerciseForm($snapshot);
    });
});

function populateExerciseForm($exerciseOBJ) {
  $ex_name = $exerciseOBJ.val().exercise_name;
  $ex_muscle = $exerciseOBJ.val().muscle_group;
  $ex_image = $exerciseOBJ.val().image;
  $ex_equipment = $exerciseOBJ.val().equipment;
  $ex_description = $exerciseOBJ.val().exercise_description;
    
  $("#ex-name").text($ex_name);
  $("#ex-muscle").text("Equipment: " + $ex_muscle);
  $("#ex-equipment").text("Muscle Group: " + $ex_equipment);
  $("#ex-description").text($ex_description);
  $("#exercise-gif").attr("src", $ex_image);
};

$("#workout-list").on("click", ".edit-ex-btn", function(){
    localStorage.setItem("workout", $(this).attr("id"));
});

$("#create-workout-button-modal").click(function(){
    localStorage.setItem("workout-name", $("#workout-name-input").val());
    localStorage.setItem("workout", "");
    $("#workout-name-input").val("");
});

//------------------------------------------------------------------------
//Send Workout Btn Press -------------------------------------------------
//------------------------------------------------------------------------
$("#workout-list").on("click", ".send-workout-btn", function(){
    sendWorkoutID = $(this).attr("id");
    
    firebase.database().ref('/athletikos_data/users/'+ firebase.auth().currentUser.uid +'/coached_teams/').once('value').then(function($snapshot) {
        $snapshot.forEach(function(child){
            firebase.database().ref('/athletikos_data/teams/'+ child.val() +'/team_name/').once('value').then(function($snapshot2) {
                var newLI = '<li><a class="team-btn-li btn" id="'+ child.val() +'">'+ $snapshot2.val() +'</a></li>';
                $("#team-dropdown-ul").append(newLI);
            });
        });
    });
    
    $sendWorkoutModal.fadeToggle($modalAnimateTime);
});

//------------------------------------------------------------------------
//Populate send workout modal once team is selected ----------------------
//------------------------------------------------------------------------
$("#team-dropdown-ul").on("click", ".team-btn-li", function(){
    $("#team-member-table").show();
    $("#member-list").html("");
    $("#send-workout-btn").prop("disabled",false);
    
    var team_id = $(this).attr("id");
    selectedTeam = team_id;

    firebase.database().ref('/athletikos_data/teams/'+ team_id +'/team_members/').once('value').then(function($snapshot) {
        $snapshot.forEach(function(child){
            firebase.database().ref('/athletikos_data/users/'+ child.val() +'/').once('value').then(function($snapshot2) {
                var newMemberRow = '<tr class="ex_table_row"> \
                                        <div class ="row"> \
                                            <td class="text-center member-name-td col-md-8" id="'+ child.val() +'">'+ $snapshot2.child("first_name").val() + ' ' + $snapshot2.child("last_name").val() +'</td> \
                                            <td class="text-center col-md-4 member-checkbox-td"><input class="member-checkbox" type="checkbox" value=""></td> \
                                        </div> \
                                    </tr>';
                
                $("#member-list").append(newMemberRow);
            });
        });
    });
});

//------------------------------------------------------------------------
//Close send-workout-modal -----------------------------------------------
//------------------------------------------------------------------------
$("#close-modal-send-workout").click(function(){
    $("#member-list").html("");
    $("#team-dropdown-ul").html("");
    $("#send-workout-btn").prop("disabled",true);
    
    $sendWorkoutModal.fadeToggle($modalAnimateTime);
    $("#send-workout-modal").modal("toggle");
    $("#team-member-table").hide();
    $("#personal-checkbox").attr("checked", false);
    $("#date-input").val("");
    $("#team-dropdown").prop("disabled",false);
});

//------------------------------------------------------------------------
//Send Workout btn click -------------------------------------------------
//------------------------------------------------------------------------
$("#send-workout-btn").click(function(){
    var check = true;
    var check2 = true;
    var dateStringArr = $("#date-input").val().split("/");
    var date_obj = {day: parseInt(dateStringArr[1]), month: parseInt(dateStringArr[0]), year: parseInt(dateStringArr[2])};
    
    if(dateStringArr[0].length != 2 || dateStringArr[1].length != 2 || dateStringArr[2].length != 4){
        check = false;
    }
    
    if(parseInt(dateStringArr[0]) > 12 || parseInt(dateStringArr[0]) <= 0 || parseInt(dateStringArr[1]) > 31 || parseInt(dateStringArr[1] <= 0) || parseInt(dateStringArr[2]) < 2017){
        check2 = false;
    }
    
    if(check){
        if(check2){
            if($("#personal-checkbox").is(":checked")){
                var workout_obj = {workout_id: sendWorkoutID, date: date_obj, owner: firebase.auth().currentUser.uid};
                firebase.database().ref().child("athletikos_data").child("users").child(firebase.auth().currentUser.uid).child("personal_workouts").child(sendWorkoutID).set(workout_obj);
            } else {
                var rows = $("#team-member-table").children("#member-list").children();

                rows.each(function(){
                    if($(this).find(".member-checkbox").is(":checked")){
                        var memberID = $(this).find(".member-name-td").attr("id");

                        var workout_obj = {workout_id: sendWorkoutID, date: date_obj, owner: selectedTeam};

                        firebase.database().ref().child("athletikos_data").child("users").child(memberID).child("team_workouts").child(sendWorkoutID).set(workout_obj);
                    }
                });
            }

            $("#member-list").html("");
            $("#team-dropdown-ul").html("");
            $("#send-workout-btn").prop("disabled",true);

            $sendWorkoutModal.fadeToggle($modalAnimateTime);
            $("#send-workout-modal").modal("toggle");
            $("#team-member-table").hide();

            $("#personal-checkbox").attr("checked", false);
            $("#date-input").val("");
            $("#team-dropdown").prop("disabled",false);

            BootstrapDialog.show({
                title: 'Your workout has been successfully sent'
            });
        } else {
            BootstrapDialog.show({
            title: 'Error',
            message: 'Please enter a valid date.'
        });
        }
    } else {
        BootstrapDialog.show({
            title: 'Error',
            message: 'The workout date must be in the following format: mm/dd/yyyy.'
        });
    }
});

//------------------------------------------------------------------------
//Personal Workour checkbox click ----------------------------------------
//------------------------------------------------------------------------
$("#personal-checkbox").click(function(){
    console.log("in");
   if(this.checked){
       $("#team-dropdown").prop("disabled",true);
       $("#send-workout-btn").prop("disabled",false);
       $("#member-list").html("");
       $("#team-member-table").hide();
   } else {
       $("#team-dropdown").prop("disabled",false);
       $("#send-workout-btn").prop("disabled",true);
   }
});

$(document).ready(function(){
    $("#tab_logic").after('<div id="empty-table-row"><h2>Your Have Created No Workouts Added</h2></div>');
    $("#team-member-table").hide();
    $("#personal-checkbox").attr("checked", false);
    $("#date-input").val("");
    $("#team-dropdown").prop("disabled",false);
    firebase.auth().onAuthStateChanged(function(user){
       if(user) {
           loadWorkouts();
           $("#send-workout-btn").prop("disabled",true);
       }
    });
});