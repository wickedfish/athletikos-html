var $currentForm = $('#exercise-form');
var $exerciseListForm = $('#exercise-list');
var $exerciseForm = $("#exercise-form");
var $divForms = $('#div-forms');
var $modalAnimateTime = 100;
var workoutID_Global = "";
var $exerciseCreation = $("#exercise-creation");


//--------------------------------------------------------------
//Pull searched exercises from firebase based on text input ---
//--------------------------------------------------------------
function pullExercises($searchString) {
  firebase.database().ref('/athletikos_data/exercises/'+ $searchString +'/').once('value').then(function($snapshot) {
      if($snapshot.val() != null){
            var $newH = $exerciseListForm.height();
            $divForms.css("height",$exerciseListForm);
            $exerciseListForm.fadeToggle($modalAnimateTime, function(){
                $divForms.animate({height: $exerciseListForm}, $modalAnimateTime, function(){
                });
            });
          
          displayExercisesList($snapshot);
      } else{
          $("#exercise-search-results").modal("toggle");
          BootstrapDialog.show({
            title: 'Error',
            message: 'The exercise searched does not exist in our database.'
          });
      }
  });

  return
};

//-----------------------------------------------
//Search button click wrapper -------------------
//-----------------------------------------------
$('#search-button').click( function (){
    pullExercises(document.getElementById("searchLabel").value);
});

//takes Firebase OBJ and populates modal
function displayExercisesList($firebaseOBJ){
    
    $ex_name = $firebaseOBJ.val().exercise_name;
    $ex_muscle = $firebaseOBJ.val().muscle_group;
    $ex_equipment = $firebaseOBJ.val().equipment;
    
    if($ex_name.length > 26){
        $ex_name = $ex_name.substring(0,25) + "...";
    }

    newTR = '<tr class="ex_table_row"> \
                    <td class="text-center" id="ex_name"> \
                        <div class ="row"> \
                            <span class="col-md-2 ex_table_item modal-list-name"><a class="ex-details-btn btn" id="'+ $ex_name +'">'+ $ex_name +'</a></span> \
                            <div class="col-md-2 ex_table_item modal-list-muscle">'+ $ex_muscle +'</div> \
                            <div class="col-md-2 ex_table_item modal-list-equip">'+ $ex_equipment +'</div> \
                            <div class="col-md-4 modal-list-btn" id="ex_buttons"><a id="'+ $ex_name +'" class="pull-right btn btn-default ex-add-btn">Add Exercise</a></div> \
                        </div> \
                    </td> \
                </tr>';

    $("#list-body").append(newTR);
};

$("#list-body").on("click", ".ex-details-btn", function(){
    $divForms.css("height",$exerciseListForm);
    $exerciseListForm.fadeToggle($modalAnimateTime, function(){
        $divForms.animate({height: $exerciseForm}, $modalAnimateTime, function(){
            $exerciseForm.fadeToggle($modalAnimateTime);
        });
    });
    
    firebase.database().ref('/athletikos_data/exercises/'+ $(this).attr('id') +'/').once('value').then(function($snapshot) {
        populateExerciseForm($snapshot);
    });
});

//---------------------------------------------
//Populate exercise details form --------------
//---------------------------------------------
function populateExerciseForm($exerciseOBJ) {
  $ex_name = $exerciseOBJ.val().exercise_name;
  $ex_muscle = $exerciseOBJ.val().muscle_group;
  $ex_image = $exerciseOBJ.val().image;
  $ex_equipment = $exerciseOBJ.val().equipment;
  $ex_description = $exerciseOBJ.val().exercise_description;
    
  $("#ex-name").text($ex_name);
  $("#ex-muscle").text("Muscle Group: " + $ex_muscle);
  $("#ex-equipment").text("Equipment: " + $ex_equipment);
  $("#ex-description").text($ex_description);
  $("#exercise-gif").attr("src", $ex_image);
  $("#ex-name").css("margin-left", "10%");
};


//---------------------------------------------
//Runs when the exerxise search modal closes --
//---------------------------------------------
$("#close-modal").click(function(){
    if($exerciseForm.is(':visible')){
        $exerciseForm.fadeToggle($modalAnimateTime);
    } else {
        $exerciseListForm.fadeToggle($modalAnimateTime);
    }
    $("#list-body").html("");
    
    if($("#add-exercise-button").is(":hidden")){
        $("#add-exercise-button").show();
    }
});

//--------------------------------------------------------
//Search using equipment filter --------------------------
//--------------------------------------------------------
$(".equipment-filter-btn").click(function(){
    var $buttonID = $(this).attr('id');
    firebase.database().ref('/athletikos_data/exercises/').once('value').then(function($snapshot) {
        var $newH = $exerciseListForm.height();
        $divForms.css("height",$exerciseListForm);
        $exerciseListForm.fadeToggle($modalAnimateTime, function(){
            $divForms.animate({height: $exerciseListForm}, $modalAnimateTime, function(){
            });
        });
        
        $snapshot.forEach(function(child){
            if(child.val().equipment == $buttonID){
                $ex_name = child.val().exercise_name;
                $ex_muscle = child.val().muscle_group;
                $ex_equipment = child.val().equipment;
                
                if($ex_name.length > 26){
                    $ex_name = $ex_name.substring(0,25) + "...";
                }

                newTR = '<tr class="ex_table_row"> \
                                <td class="text-center" id="ex_name"> \
                                    <div class ="row"> \
                                        <span class="col-md-2 ex_table_item modal-list-name"><a class="ex-details-btn btn" id="'+ $ex_name +'">'+ $ex_name +'</a></span> \
                                        <div class="col-md-2 ex_table_item modal-list-muscle">'+ $ex_muscle +'</div> \
                                        <div class="col-md-2 ex_table_item modal-list-equip">'+ $ex_equipment +'</div> \
                                        <div class="col-md-4 modal-list-btn" id="ex_buttons"><a id="'+ $ex_name +'" class="pull-right btn btn-default ex-add-btn">Add Exercise</a></div> \
                                    </div> \
                                </td> \
                            </tr>';

                $("#list-body").append(newTR);
            }
        });
    });
});

//--------------------------------------------------------
//Search using muscele group filter ----------------------
//--------------------------------------------------------
$(".muscle-filter-btn").click(function(){
    var $buttonID = $(this).attr('id');
    firebase.database().ref('/athletikos_data/exercises/').once('value').then(function($snapshot) {
        var $newH = $exerciseListForm.height();
        $divForms.css("height",$exerciseListForm);
        $exerciseListForm.fadeToggle($modalAnimateTime, function(){
            $divForms.animate({height: $exerciseListForm}, $modalAnimateTime, function(){
            });
        });
        
        $snapshot.forEach(function(child){
            if(child.val().muscle_group == $buttonID){
                $ex_name = child.val().exercise_name;
                $ex_muscle = child.val().muscle_group;
                $ex_equipment = child.val().equipment;
                
                if($ex_name.length > 26){
                    $ex_name = $ex_name.substring(0,25) + "...";
                }

                newTR = '<tr class="ex_table_row"> \
                                <td class="text-center" id="ex_name"> \
                                    <div class ="row"> \
                                        <span class="col-md-2 ex_table_item modal-list-name"><a class="ex-details-btn btn" id="'+ $ex_name +'">'+ $ex_name +'</a></span> \
                                        <div class="col-md-2 ex_table_item modal-list-muscle">'+ $ex_muscle +'</div> \
                                        <div class="col-md-2 ex_table_item modal-list-equip">'+ $ex_equipment +'</div> \
                                        <div class="col-md-4 modal-list-btn" id="ex_buttons"><a id="'+ $ex_name +'" class="pull-right btn btn-default ex-add-btn">Add Exercise</a></div> \
                                    </div> \
                                </td> \
                            </tr>';

                $("#list-body").append(newTR);
            }
        });
    });
});

//--------------------------------------------------------
//Add exercise from exercise details modal ---------------
//--------------------------------------------------------
$("#add-exercise-button").click(function(){
    var ex_name = $("#ex-name").text();
    $("#empty-table-row").remove();
    newTR = '<tr class="ex_table_row ex-tr"> \
                    <td id="row_number" align="center"> <input type="image" class="ex-up-btn" src="./workoutViewPage/up-arrow.png" style="width:30px;height:30px;"> <input type="image" class="ex-down-btn" src="./workoutViewPage/down-arrow.png" style="width:30px;height:30px;"> </td> \
                    <td class="col-md-2"  id="name" align="center"><a class="ex-details-btn btn for-text" data-toggle="modal" data-target="#exercise-search-results" id="'+ $ex_name +'">'+ $ex_name +'</a></td> \
                    <td class="col-md-2 ex_table_item" id="reps" align="center"><input type="text" class="form-control ex-input" value=""></td> \
                    <td class="col-md-2 ex_table_item" id="weight" align="center"><input type="text" class="form-control ex-input" value=""></td> \
                    <td class="col-md-2 ex_table_item" id="sets" align="center"><input type="text" class="form-control ex-input" value=""></td> \
                    <td class="col-md-4" id="ex_buttons" align="center"><a class="ex-delete-btn btn btn-default  btn-delete-exercise">Remove Exercise</a></td> \
                </tr>';
        
     $("#workout-exercise-list").append(newTR);
     $("#save-workout-btn").prop("disabled",false);
});


//--------------------------------------------------------
//Add exercise click from exercise search results modal --
//--------------------------------------------------------
$("#list-body").on("click", ".ex-add-btn", function(){
    var ex_name = $(this).attr("id");
    $("#empty-table-row").remove();
    newTR = '<tr class="ex_table_row ex-tr"> \
                    <td id="row_number" align="center"> <input type="image" class="ex-up-btn" src="./workoutViewPage/up-arrow.png" style="width:30px;height:30px;"> <input type="image" class="ex-down-btn" src="./workoutViewPage/down-arrow.png" style="width:30px;height:30px;"> </td> \
                    <td class="col-md-2"  id="name" align="center"><a class="ex-details-btn btn for-text" data-toggle="modal" data-target="#exercise-search-results" id="'+ $ex_name +'">'+ $ex_name +'</a></td> \
                    <td class="col-md-2 ex_table_item" id="reps" align="center"><input type="text" class="form-control ex-input" value=""></td> \
                    <td class="col-md-2 ex_table_item" id="weight" align="center"><input type="text" class="form-control ex-input" value=""></td> \
                    <td class="col-md-2 ex_table_item" id="sets" align="center"><input type="text" class="form-control ex-input" value=""></td> \
                    <td class="col-md-4" id="ex_buttons" align="center"><a class="ex-delete-btn btn btn-default  btn-delete-exercise">Remove Exercise</a></td> \
                </tr>';
        
     $("#workout-exercise-list").append(newTR);
     $("#save-workout-btn").prop("disabled",false);
});

//--------------------------------------------------------
//Exercise Details click from workout exercise list-------
//--------------------------------------------------------
$("#workout-exercise-list").on('click', '.ex-details-btn', function(){
    var ex_name = $(this).attr("id");
    console.log("here");
    firebase.database().ref('/athletikos_data/exercises/'+ ex_name +'/').once('value').then(function($snapshot) {
        $ex_name = $snapshot.val().exercise_name;
        $ex_muscle = $snapshot.val().muscle_group;
        $ex_image = $snapshot.val().image;
        $ex_equipment = $snapshot.val().equipment;
        $ex_description = $snapshot.val().exercise_description;

        $("#ex-name").text($ex_name);
        $("#ex-muscle").text("Muscle Group: " + $ex_muscle);
        $("#ex-equipment").text("Equipment: " + $ex_equipment);
        $("#ex-description").text($ex_description);
        $("#exercise-gif").attr("src", $ex_image);
        $("#ex-name").css("margin-left", "32%");
        
        $("#add-exercise-button").hide();
    });
    
     $exerciseForm.fadeToggle($modalAnimateTime);
});

//--------------------------------------------------------
//Exercise Delete click from workout exercise list-------
//--------------------------------------------------------
$("#workout-exercise-list").on('click', '.ex-delete-btn', function(){
    $(this).closest("tr").remove();
    $("#save-workout-btn").prop("disabled",false);
    if($("#workout-exercise-list").html() == ""){
        $("#tab_logic").after('<div id="empty-table-row"><h2>No Exercises Added</h2></div>');
    }
});

$(".ex_table_item").on("input", function() {
    $("#save-workout-btn").prop("disabled",false);
});

//--------------------------------------------------
//Save workout feature starts HERE------------------
//--------------------------------------------------
$("#save-workout-btn").click(function(){
    
    if($("#workout-exercise-list").children().length > 0){
        var ex_count = 0;
        var ex_list_obj = {};
        var inValidInput = false;
        
       $("#workout-exercise-list").children(".ex_table_row").each(function(){
            var single_ex = {};
            $(this).contents().each(function(){
                if($(this).attr("id") == "name"){
                    single_ex["exercise_name"] = $(this).find(".for-text").text();
                } else if($(this).attr("id") == "ex_buttons"){
                    //nothing
                } else if($(this).attr("id") == "exercise_placement"){
                    single_ex[$(this).attr("id")] = parseInt($(this).text());
                }
                else if($(this).attr("class") == "col-md-2 ex_table_item"){
                    if($.isNumeric($(this).find(".ex-input").val()) && $(this).find(".ex-input").val() != ""){
                        single_ex[$(this).attr("id")] = parseInt($(this).find(".ex-input").val());
                    } else {
                        inValidInput = true;
                    }
                }
            });
            
            ex_list_obj[ex_count] = single_ex;
            
            ex_count++;
        });
        if(!inValidInput){
            var workout_obj = {"workout_name": $("#workout-name").text(), "exercises": ex_list_obj};
            
            if(workoutID_Global == ""){
                //create new ID for workout under WORKOUTS section of data base
                var newID = firebase.database().ref().child("athletikos_data").child("workouts").push(workout_obj).key;
                workoutID_Global = newID;
                console.log(newID);
                //store workout id under user's workouts
                firebase.database().ref().child("athletikos_data").child("users").child(firebase.auth().currentUser.uid).child("workouts").push(newID);
            } else {
                firebase.database().ref().child("athletikos_data").child("workouts").child(workoutID_Global).set(workout_obj);
            }
            
            $("#save-workout-btn").prop("disabled",true);
            
            BootstrapDialog.show({
                title: '',
                message: 'Workout Saved.'
            });
        } else {
            BootstrapDialog.show({
                title: 'Error',
                message: 'Make sure your exercise data is numerical and non empty.'
            });
        }
    } else {
        BootstrapDialog.show({
            title: 'Error',
            message: 'You need to add exercises to your workout before you can save.'
        });
    }
});

function loadWorkout(){
    firebase.database().ref('/athletikos_data/workouts/'+localStorage.getItem("workout")).once('value').then(function($snapshot) {
        $("#workout-name").text($snapshot.val().workout_name);
        
        $snapshot.val().exercises.forEach(function(child){
            console.log(child.exercise_name);
            console.log(child.sets);
            $ex_name = child.exercise_name;
            $ex_sets = child.sets;
            $ex_weight = child.weight;
            $ex_reps = child.reps;

            newTR = '<tr class="ex_table_row ex-tr"> \
                <td id="row_number" align="center"> <input type="image" class="ex-up-btn" src="./workoutViewPage/up-arrow.png" style="width:30px;height:30px;"> <input type="image" class="ex-down-btn" src="./workoutViewPage/down-arrow.png" style="width:30px;height:30px;"> </td> \
                <td class="col-md-2"  id="name" align="center"><a class="ex-details-btn btn for-text" data-toggle="modal" data-target="#exercise-search-results" id="'+ $ex_name +'">'+ $ex_name +'</a></td> \
                <td class="col-md-2 ex_table_item" id="reps" align="center"><input type="text" class="form-control ex-input" value='+$ex_reps+'></td> \
                <td class="col-md-2 ex_table_item" id="weight" align="center"><input type="text" class="form-control ex-input" value='+$ex_weight+'></td> \
                <td class="col-md-2 ex_table_item" id="sets" align="center"><input type="text" class="form-control ex-input" value='+$ex_sets+'></td> \
                <td class="col-md-4" id="ex_buttons" align="center"><a class="ex-delete-btn btn btn-default  btn-delete-exercise">Remove Exercise</a></td> \
            </tr>';
        
            $("#workout-exercise-list").append(newTR);
            
        });
    });
}

//------------------------------------------------------------------
//Up and Down arrow click functions---------------------------------
//------------------------------------------------------------------
$("#workout-exercise-list").on("click", ".ex-up-btn", function(){
    //store exercise from .ex-wrapper above clicked wrapper
    var children = $(this).closest(".ex_table_row").prev().children();
    
    //remove exercise from .ex-wrapper above clicked wrapper
    $(this).closest(".ex_table_row").prev().empty();
    
    //Append the clicked wrapper's exercise to the above wrapper
    $(this).closest(".ex_table_row").prev().append($(this).closest(".ex_table_row").children());
    
    //Append the upper exercise to the clicked wrapper
    $(this).closest(".ex_table_row").next().append(children);
});

$("#workout-exercise-list").on("click", ".ex-down-btn", function(){
    //store exercise from .ex-wrapper below clicked wrapper
    var children = $(this).closest(".ex_table_row").next().children();
    
    //remove exercise from .ex-wrapper below clicked wrapper
    $(this).closest(".ex_table_row").next().empty();
    
    //Append the clicked wrapper's exercise to the above wrapper
    $(this).closest(".ex_table_row").next().append($(this).closest(".ex_table_row").children());
    
    //Append the upper exercise to the clicked wrapper
    $(this).closest(".ex_table_row").prev().append(children);
});

//------------------------------------------------------------------
//Closing Exercise Creation Modal-----------------------------------
//------------------------------------------------------------------
$("#close-modal-ex-creation").click(function(){
    $("#ex-create-name").val("");
    $("#ex-create-equip").val("");
    $("#ex-create-description").val("");
    $("#ex-create-muscle-group").val("");
    $exerciseCreation.fadeToggle($modalAnimateTime);
});

//------------------------------------------------------------------
//Closing Exercise Creation Modal-----------------------------------
//------------------------------------------------------------------
$("#create-exercise-button").click(function(){
    $divForms.css("height",$exerciseCreation);
    $exerciseCreation.fadeToggle($modalAnimateTime);
});

//------------------------------------------------------------------
//Save New Exercise ------------------------------------------------
//------------------------------------------------------------------
$("#submit-new-exercise").click(function(){
     if($("#ex-create-name").val() != "" && $("#ex-create-equip").val() != "" && $("#ex-create-description").val() != "" && $("#ex-create-muscle-group").val() != ""){
         
        var workout_obj = {exercise_name: $("#ex-create-name").val(), equipment: $("#ex-create-equip").val(), image: "https://cdn.pixabay.com/photo/2016/08/31/22/21/dumbbells-1634750_960_720.jpg", muscle_group: $("#ex-create-muscle-group").val(), exercise_description: $("#ex-create-description").val()};
            
        firebase.database().ref().child("athletikos_data").child("exercises").child($("#ex-create-name").val()).set(workout_obj);
            
        BootstrapDialog.show({
            title: '',
            message: 'Exercise Saved.'
        });
         
        $("#ex-create-name").val("");
        $("#ex-create-equip").val("");
        $("#ex-create-description").val("");
        $("#ex-create-muscle-group").val("");
        $exerciseCreation.fadeToggle($modalAnimateTime);
        $('#exercise-creation-modal').modal('hide');
        
    } else {
        BootstrapDialog.show({
            title: 'Error',
            message: 'You need to fill in all exercise information before you can save.'
        });
    }
});

//------------------------------------------------------------------
//Document Ready----------------------------------------------------
//------------------------------------------------------------------
$(document).ready(function(){
    $("#save-workout-btn").prop("disabled",false);
    
    //if a workout has been selected to edit
    if(localStorage.getItem("workout") != ""){
        workoutID_Global = localStorage.getItem("workout");
        loadWorkout();
    //if a new workout is being created
    } else {
        $("#workout-name").text(localStorage.getItem("workout-name"));
        $("#tab_logic").after('<div id="empty-table-row"><h2>No Exercises Added</h2></div>');
    }
});

