localStorage.setItem("teamID", null);
$(function () {

    var $formCreate = $('#create-team-form');
    var $formJoin = $('#join-team-form');
    var $divForms = $('#div-forms');
    var $modalAnimateTime = 100;
    var $msgAnimateTime = 150;
    var $msgShowTime = 2000;




    $('#create_a_team_btn').click(function () { modalAnimate($formCreate, $formJoin) });
    $('#join_a_team_btn').click(function () { modalAnimate($formJoin, $formCreate) });
    $('#join_submit').click(function () {
        var user = firebase.auth().currentUser.uid;
        //add team to user
        var x = firebase.database().ref('/athletikos_data/users/' + user + "/subscribed_teams/")

        var thing = $('#join_code').val();
        x.push(thing);
        //add user to team
        x = firebase.database().ref('/athletikos_data/teams/' + thing + "/team_members");
        x.push(user);
    });

    $('#create_send').click(function () {
        var user = firebase.auth().currentUser.uid;
        var name = $('#team_name').val();

        //build team
        var y = firebase.database().ref('/athletikos_data/teams/');
        //keep id for use with the user
        var id = y.push({ 'team_name': name, 'coach_id': user });
        var z = id.key;
        console.log(z);

        //add coach to list of members
        y = firebase.database().ref('/athletikos_data/teams/' + z + '/team_members');
        y.push(user);





        //add team to user
        var x = firebase.database().ref('/athletikos_data/users/' + user + "/subscribed_teams/");


        x.push(z);
        //add team to coached teams
        x = firebase.database().ref('/athletikos_data/users/' + user + "/coached_teams/");
        x.push(z);



        window.location = "TeamList.html";
    });

    function modalAnimate($oldForm, $newForm) {
        var $oldH = $oldForm.height();
        var $newH = $newForm.height();
        $divForms.css("height", $oldH);
        $oldForm.fadeToggle($modalAnimateTime, function () {
            $divForms.animate({ height: $newH }, $modalAnimateTime, function () {
                $newForm.fadeToggle($modalAnimateTime);
            });
        });
    }
});
firebase.auth().onAuthStateChanged(function (user) {
    // if user is signed in
    if (user) {
        // load user's teams
        var user = firebase.auth().currentUser.uid;

        firebase.database().ref('/athletikos_data/users/' + user + "/").child("subscribed_teams").once('value').then(function (snapshot) {
            //iterate over every team
            snapshot.forEach(function (child) {
                firebase.database().ref('/athletikos_data/teams/' + child.val() + "/").once('value').then(function (snapshot) {
                    $('.list-group').append('<a href="TeamPage.html" id="'+child.val() +'" class="list-group-item">' + snapshot.val().team_name + '<button class="pull-right btn-sm btn-primary btn leave" id="' + child.val() + '">Leave Team</button></a>');
                });
            });

        });
    }
    // if user is signed out
    else {
        $('.list-group').html("");
        localStorage.clear();
    }
});

$(document.body).on('click', '.leave', function (e) {
    e.preventDefault();
    e.stopPropagation();

    //if the user is the coach, delete the entire team
    firebase.database().ref('/athletikos_data/teams/' + e.target.id + '/coach_id').once('value').then(function (snapshot) {
        if (snapshot.val() == firebase.auth().currentUser.uid) {
            //    remove from coached teams
            firebase.database().ref('/athletikos_data/users/' + firebase.auth().currentUser.uid + '/coached_teams/').once('value').then(function (teams) {
                teams.forEach(function (rm) {
                    if (rm.val() == e.target.id) {
                        rm.ref.remove();
                    }
                });

            });

            // remove from subscribed teams
            firebase.database().ref('/athletikos_data/users/' + firebase.auth().currentUser.uid + '/subscribed_teams/').once('value').then(function (teams) {
                teams.forEach(function (rm) {
                    if (rm.val() == e.target.id) {
                        rm.ref.remove();
                    }
                });
                window.location = "TeamList.html";
            });
        }
        else {
            // otherwise remove just the user
            firebase.database().ref('/athletikos_data/teams/' + e.target.id + '/team_members').once('value').then(function (snapshot) {
                var rm = new firebase.database().ref('/athletikos_data/teams/' + e.target.id + '/team_members/' + firebase.auth().currentUser.uid + '/');
                rm.remove(function (error) {

                });
                window.location = "TeamList.html";
            });
        }

    });




});

$(document.body).on('click', '.list-group-item', function (e) {
    console.log("here");
    e.preventDefault();
    var user = firebase.auth().currentUser.uid;

    firebase.database().ref('/athletikos_data/users/' + user + "/").child("subscribed_teams").once('value').then(function (snapshot) {
        //iterate over every team
        snapshot.forEach(function (child) {
            firebase.database().ref('/athletikos_data/teams/' + child.val() + "/").once('value').then(function (snapshot) {
                if (snapshot.key == e.target.id) {
                    localStorage.setItem("teamID", child.val());
                    window.location = "TeamPage.html";

                }
            });
        });

    });

});

$(".LoginButton").click(function () {
    window.location = "MainPage.html";
});
